/* matt sucks major dicks */
#include "/usr/avr/include/avr/io.h"
#include "/usr/avr/include/avr/interrupt.h"
#include "/usr/avr/include/stdlib.h"
#include "/usr/avr/include/stdio.h"
#include "/usr/avr/include/util/delay.h"
#include "/usr/avr/include/avr/pgmspace.h"
#include "latedef.h"

/* define fixed stack size for a task */
#define STACK_SIZE 128
/* 2 bytes each; r26, r27 used for storing stack pointer stuff, current and prev */
#define SIZEOF_SWITCH_STACK_DROP 4

#define TCCR0B _SFR_IO8(0x25)
#define CS00 0
#define CS01 1
#define CS02 2

/* clear bit/set bit macros */
/* old cbi & sbi macros deprecated, with the below compiler optimizer will use hardware sbi if available */
#ifndef _cbi
#define _cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef _sbi
#define _sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

/* define a task block */

struct _ATB
{
  char *stackptr;
  struct _ATB *prev_t;
  struct _ATB *next_t;
  char stack[STACK_SIZE];
  int task;
  /* int (*task)(void); */
  /* perhaps replace int task with the above, have dereferenced void function
     struct member pass to functions instead of functions taking (*void)task()  */ 
};

/* struct _ATB; */
typedef struct _ATB ATB;

/* task pointers and fn prototypes */
ATB *task_to_del = NULL;
ATB *task_head = NULL;
ATB *task_tail;
ATB *task_current;

/* task_to_del = NULL; */
/* task_head = NULL; */

void registers_save(void);
void registers_restore(void);
void timer_init_isr(void);
int spawn_task(void (*task)());
void delete_task(void (*task)());
void ticktimer_isr(void);
void main(void);

/* save registers and current stack pointer */
void registers_save(void)
{
  __asm__ __volatile__ (	"push	r0						\n\t" 
				"in	r0, SREG			                \n\t" 
				"cli							\n\t" 
				"push	r0						\n\t" 
				"push	r1						\n\t" 
				"clr	r1						\n\t" 
				"push	r2						\n\t" 
				"push	r3						\n\t" 
				"push	r4						\n\t" 
				"push	r5						\n\t" 
				"push	r6						\n\t" 
				"push	r7						\n\t" 
				"push	r8						\n\t" 
				"push	r9						\n\t" 
				"push	r10						\n\t" 
				"push	r11						\n\t" 
				"push	r12						\n\t" 
				"push	r13						\n\t" 
				"push	r14						\n\t" 
				"push	r15						\n\t" 
				"push	r16						\n\t" 
				"push	r17						\n\t" 
				"push	r18						\n\t" 
				"push	r19						\n\t" 
				"push	r20						\n\t" 
				"push	r21						\n\t" 
				"push	r22						\n\t" 
				"push	r23						\n\t" 
				"push	r24						\n\t" 
				"push	r25						\n\t" 
				"push	r26						\n\t" 
				"push	r27						\n\t" 
				"push	r28						\n\t" 
				"push	r29						\n\t" 
				"push	r30						\n\t" 
				"push	r31						\n\t" 
				"lds	r26, task_current		                \n\t" 
				"lds	r27, task_current + 1                    	\n\t" 
				"in	r0, 0x3d					\n\t" 
				"st	x+, r0						\n\t" 
				"in	r0, 0x3e					\n\t" 
				"st	x+, r0						\n\t" 
    );
}

/* next stack pointer and restore registers */
void registers_restore(void)
{
  __asm__ __volatile__ (	"lds	r26, task_current				\n\t" 
				"lds	r27, task_current + 1                      	\n\t" 
				"ld	r28, x+						\n\t" 
				"out	__SP_L__, r28					\n\t" 
				"ld	r29, x+						\n\t" 
				"out	__SP_H__, r29					\n\t" 
				"pop	r31						\n\t" 
				"pop	r30						\n\t" 
				"pop	r29						\n\t" 
				"pop	r28						\n\t" 
				"pop	r27						\n\t" 
				"pop	r26						\n\t" 
				"pop	r25						\n\t"
				"pop	r24						\n\t" 
				"pop	r23						\n\t" 
				"pop	r22						\n\t" 
				"pop	r21						\n\t" 
				"pop	r20						\n\t" 
				"pop	r19						\n\t" 
				"pop	r18						\n\t" 
				"pop	r17						\n\t" 
				"pop	r16						\n\t" 
				"pop	r15						\n\t" 
				"pop	r14						\n\t" 
				"pop	r13						\n\t" 
				"pop	r12						\n\t" 
				"pop	r11						\n\t" 
				"pop	r10						\n\t" 
				"pop	r9						\n\t" 
				"pop	r8						\n\t" 
				"pop	r7						\n\t" 
				"pop	r6						\n\t" 
				"pop	r5						\n\t" 
				"pop	r4						\n\t" 
				"pop	r3						\n\t" 
				"pop	r2						\n\t" 
				"pop	r1						\n\t" 
				"pop	r0						\n\t" 
				"out	__SREG__, r0					\n\t" 
				"pop	r0						\n\t"
    );
}

/* initialize timer tick for the ISR  */
void timer_init_isr(void)
{
  _sbi(TCCR0B, CS02);
  _sbi(TCCR0B, CS00);
  
#if defined(TIMSK) && defined(TOIE0)
  _sbi(TIMSK, TOIE0);
#elif defined(TIMSK0) && defined(TOIE0)
  _sbi(TIMSK0, TOIE0);
#else
#endif
}

/* handles creation of task block */
int spawn_task(void (*task)())
{
  /* struct _ATB *newTB = NULL; */
  ATB *newTB;
  newTB = NULL;
  newTB = (struct _ATB*)malloc((sizeof(struct _ATB)));

  /* safety net */
  if(newTB==NULL)
  {
    return 0; /* if task hasn't or can't been created get out of here */
  }

  memset(((void *)newTB), 0, sizeof(ATB));

  /* save task address and put new task on stack */
  newTB->this_task = (int)task;
  *(int *)((newTB->this_task) + (STACK_SIZE - SIZEOF_SWITCH_STACK_DROP)) = (int)task;

  /* load task pointer accounting for the context switch stack drop */
  newTB->stackptr = (newTB->stack) + (STACK_SIZE - SIZEOF_SWITCH_STACK_DROP);

  /* if first task */
  if(task_head == NULL)
  {
    task_head = newTB;
    task_tail = newTB;

    /* if it's the first task it's currently the only task */
    task_current = newTB;
  }

  /* make pointers go to the right places */
  newTB->prev_t = task_tail;
  task_tail->next_t = newTB;
  newTB->next_t = task_head;
  task_tail = newTB;
  task_head->prev_t = task_tail;
  
  return 1;
}

/* handle marking tasks for deletion */
void delete_task(void (*task)())
{
  ATB *tblock_item = task_head;

  if(tblock_item==NULL)
  {
    return; /* nothing to do here */
  }

  do
  {
    if((int)task == tblock_item->this_task)
    {
      if((int)task == task_head->this_task)
      {
	return; /* failure upon trying to delete the head task */
      }

      /* reorder the list */
      tblock_item->prev_t->next_t = tblock_item->next_t;
      tblock_item->next_t->prev_t = tblock_item->prev_t;

      task_to_del = tblock_item; /* mark the task for deletion */
      return; /* done */
    }
    tblock_item = tblock_item->next_t;
  } while(tblock_item != task_head); 
}

/* interrupt service */
void ticktimer_isr(void)
{
  /* disable all interrupts */
  cli(); 

  if(task_to_del == NULL)
  {
    /* save registers and current stack pointer */
    registers_save();
    task_current = task_current->next_t;
  }
  else
  {
    task_current = task_current->next_t;

    /* remove the task block of the finished task */
    free((void *)task_to_del);
    task_to_del = NULL;
  }

  /* load next task's stack, restore registers, enable interrupts, return */
  registers_restore();
  sei();
  return;
}

void main(void)
{
  return 0;
}
