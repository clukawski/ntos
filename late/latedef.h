#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

/* define fixed stack size for a task */
#define STACK_SIZE 128
#define SIZEOF_SWITCH_STACK_DROP 2

/* clear bit/set bit macros */
/* old cbi & sbi macros deprecated, with the below compiler optimizer will use hardware sbi if available */
#ifndef _cbi
#define _cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef _sbi
#define _sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

/* define a task block */
extern typedef struct _ATB
{
   char *stackptr;
   struct _ATB *prev_t;
   struct _ATB *next_t;
   char stack[STACK_SIZE];
   int task;
} ATB;

/* and now some commonly used task pointers */
extern ATB *task_to_end;
extern ATB *task_head;
extern ATB *task_tail;
extern ATB *task_current;

/* save registers and current stack pointer */
extern void registers_save(void);

/* next stack pointer and restore registers */
extern void registers_restore(void);

/* initialize timer tick for the ISR  */
extern void timer_init_isr(void);
